<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="model.Empresa"%>
<%@ page import="service.UsuarioService"%>
<%@ page import="dao.UsuarioDAO"%>

<!-- Importando o header -->
<c:import url="header-empresa.jsp" />

<script>
	jQuery(document).ready(function($) {
		$('body').addClass('page-login');
	});
</script>
<!-- Conteúdo desta página -->

<main>
	<h1>Cadastrar Empresa</h1>
	<div class="row">
		<div id="terms" class="col-md-12 box animated fadeInRight">
				</div>

			</div>
			<div id="after-terms">
				<div class="col-md-12 box">
					<h3>Dados da empresa:</h3>
					<form action="methods/registroEmp.jsp">
						<input type="hidden" name="id" value="${empresa.id }" />
						<input type="text" name="cnpj"  class="col-md-3" required="required" placeholder="CNPJ">
						<input type="text" name="razaoSocial" class="col-md-4" required="required" placeholder="Razão Social">
						<input type="text" name="nome" class="col-md-3" required="required" placeholder="Nome">
						<input type="text" name="telefone" class="col-md-4" required="required" placeholder="Telefone">
						<input type="text" name="email" class="col-md-4" required="required" placeholder="E-mail">
						<input type="text" name="numero" class="col-md-4" required="required" placeholder="Numero">
						<input type="text" name="cep" class="col-md-4" required="required" placeholder="CEP">
						<input type="text" name="logradouro" class="col-md-4" required="required" placeholder="Logradouro">
						<input type="text" name="bairro" class="col-md-4" required="required" placeholder="Bairro">
						<input type="text" name="estado" class="col-md-4" required="required" placeholder="Estado">
						<input type="text" name="cidade" class="col-md-4" required="required" placeholder="Cidade">
						<button type="submit" class="pull-right" name="acao" value="Alterar">Cadastrar</button>
					</form>
					
				</div>
			</div>
		</div>
	</main>
	<!-- Importando o Footer -->
	<c:import url="footer.jsp" />