package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.CartorioService;
import model.Cartorio;


@WebServlet("/listar_cartorios.do")
public class ListarCartoriosController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public ListarCartoriosController() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String chave = request.getParameter("data[search]");
		String acao = request.getParameter("acao");
		CartorioService criador = new CartorioService();
		ArrayList<Cartorio> lista = null;
		HttpSession session = request.getSession();
		if (acao.equals("buscar")) {
		if (chave != null && chave.length() > 0) {
		lista = criador.listarCartorios(chave);
		} else {
		lista = criador.listarCartorios();
		}session.setAttribute("lista", lista);
		} else if (acao.equals("reiniciar")) {
		session.setAttribute("lista", null);
		}
		RequestDispatcher dispatcher = request
		.getRequestDispatcher("lista-cartorios.jsp");
		dispatcher.forward(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
