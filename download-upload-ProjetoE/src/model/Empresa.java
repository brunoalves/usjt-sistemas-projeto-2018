package model;

import java.io.Serializable;

public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String cnpj;
		private String senha;
		private String situacaoCad;
		private String razaoSocial;
		private String nome;
		private String documento;
		private int telefone;
		private String email;
		private String numero;
		private String CEP;
		
		public Empresa() {
		}
		
		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getCnpj() {
			return cnpj;
		}



		public void setCnpj(String cnpj) {
			this.cnpj = cnpj;
		}



		public String getRazaoSocial() {
			return razaoSocial;
		}



		public void setRazaoSocial(String razaoSocial) {
			this.razaoSocial = razaoSocial;
		}



		public String getNome() {
			return nome;
		}



		public void setNome(String nome) {
			this.nome = nome;
		}



		public String getDocumento() {
			return documento;
		}



		public void setDocumento(String documento) {
			this.documento = documento;
		}



		public int getTelefone() {
			return telefone;
		}



		public void setTelefone(int telefone) {
			this.telefone = telefone;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}
		public String getSenha() {
			return senha;
		}

		public void setSenha(String senha) {
			this.senha = senha;
		}

		public String getSituacaoCad() {
			return situacaoCad;
		}

		public void setSituacaoCad(String situacaoCad) {
			this.situacaoCad = situacaoCad;
		}

		public String getNumero() {
			return numero;
		}

		public void setNumero(String numero) {
			this.numero = numero;
		}

		public String getCEP() {
			return CEP;
		}

		public void setCEP(String cEP) {
			CEP = cEP;
		}
		

		@Override
		public String toString() {
			return "Id= " + id + ", \nC.N.P.J= " + cnpj + "\nRazao Social= " + razaoSocial
					+ "\nNome= " + nome + "\nDocumento= " + documento + "\nTelefone= " + telefone + "\nE-mail= " + email + "\n-------------";
		}

		
}	