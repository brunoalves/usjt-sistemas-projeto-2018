package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.sql.Statement;

import model.Usuario;
import dao.ConnectionFactory;


public class UsuarioDAO {

		//METODO C R U D
			
			



			public int criar(Usuario usuario) {
				String sqlInsert = "INSERT INTO usuario(documento, senha, situacaoCad) "
						+ "VALUES (?, ?, ?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, usuario.getDocumento());
					stm.setString(2, usuario.getSenha());
					stm.setString(3, usuario.getSituacaoCad());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							usuario.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return usuario.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM usuario WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Usuario usuario) {

				String sqlUpdate = "UPDATE empresa SET documento=?, senha=?, situacaoCad=?"
						+ " WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, usuario.getDocumento());
					stm.setString(2, usuario.getSenha());
					stm.setString(3, usuario.getSituacaoCad());

					stm.setInt(4, usuario.getId());
				stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			
			public Usuario carregar(Usuario usuario) {
				String sqlSelect = "SELECT documento, senha, situacaoCad"
						+ "  FROM usuario WHERE Usuario.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, usuario.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							usuario.setDocumento(rs.getString("nome"));
							usuario.setSenha(rs.getString("senha"));
							usuario.setSituacaoCad(rs.getString("situacaoCad"));

						} else {
							usuario.setId(-1);
							usuario.setDocumento(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return usuario;
			}
			


	public boolean verificarLogin(String usuario, String Senha) {
		String sqlSelect = "SELECT documento, senha, situacaoCad"
				+ "  FROM usuario WHERE Usuario.id = ?";
		
		try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
			
			try (ResultSet rs = stm.executeQuery();) {
				if (rs.next()) {
					return true;

				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e1) {
			System.out.print(e1.getStackTrace());
	}
		return false;		
		
		
	}

}
	
			
			
			
		
			
			

