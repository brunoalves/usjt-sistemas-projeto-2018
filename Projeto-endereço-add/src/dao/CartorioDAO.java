package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Cartorio;
import model.Empresa;


public class CartorioDAO {
	

	

		//METODO C R U D
			
			public int criar(Cartorio cartorio) {
				String sqlInsert = "INSERT INTO cartorio(cnpj, nome, telefone, horarioFunc, site, numero, CEP, logradouro, bairro, cidade, estado) "
						+ "VALUES (?, ?, ?, ?, ?,? ,? ,? ,? ,? ,?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, cartorio.getCnpj());
					stm.setString(2, cartorio.getNome());
					stm.setInt(3, cartorio.getTelefone());
					stm.setString(4, cartorio.getHorarioFunc());
					stm.setString(5, cartorio.getSite());
					stm.setInt(5, cartorio.getNumero());
					stm.setInt(6, cartorio.getCEP());
					stm.setString(7, cartorio.getLogradouro());
					stm.setString(8, cartorio.getBairro());
					stm.setString(9, cartorio.getCidade());
					stm.setString(10, cartorio.getEstado());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							cartorio.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return cartorio.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM cartorio WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Cartorio cartorio) {
				String sqlUpdate = "UPDATE cartorio SET cnpj=?, nome=?, telefone=?, horarioFunc=?, site=?, numero=?, cep=?, logradouro=?, bairro=?, cidade=?,estado=? "
						+ " WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, cartorio.getCnpj());
					stm.setString(2, cartorio.getNome());
					stm.setInt(3, cartorio.getTelefone());
					stm.setString(4, cartorio.getHorarioFunc());
					stm.setString(5, cartorio.getSite());
					stm.setInt(5, cartorio.getNumero());
					stm.setInt(6, cartorio.getCEP());
					stm.setString(7, cartorio.getLogradouro());
					stm.setString(8, cartorio.getBairro());
					stm.setString(9, cartorio.getCidade());
					stm.setString(10, cartorio.getEstado());

					stm.setInt(4, cartorio.getId());
				stm.execute();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
			
			
			
			public Cartorio carregar(Cartorio cartorio) {
				String sqlSelect = "SELECT cnpj, nome, telefone, horarioFunc, site, numero, cep, logradouro,bairro, cidade, estado"
						+ "  FROM cartorio WHERE Cartorio.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, cartorio.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							cartorio.setCnpj(rs.getString("nome"));
							cartorio.setNome(rs.getString("nome"));
							cartorio.setTelefone(rs.getInt("nome"));
							cartorio.setHorarioFunc(rs.getString("nome"));
							cartorio.setSite(rs.getString("nome"));
							stm.setInt(5, cartorio.getNumero());
							stm.setInt(6, cartorio.getCEP());
							stm.setString(7, cartorio.getLogradouro());
							stm.setString(8, cartorio.getBairro());
							stm.setString(9, cartorio.getCidade());
							stm.setString(10, cartorio.getEstado());

						} else {
							cartorio.setId(-1);
							cartorio.setNome(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return cartorio;
			}
			
			
			public ArrayList<Cartorio> listarCartorios() {
				Cartorio cartorio;
				ArrayList<Cartorio> lista = new ArrayList<>();
				String sqlSelect = "SELECT id, cnpj, nome, documento, telefone, situacaoCad, horarioFunc, site, numero, cep, logradouro, bairro, cidade, estado FROM cartorio";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
				try (ResultSet rs = stm.executeQuery();) {
				while (rs.next()) {
				cartorio = new Cartorio();
				cartorio.setId(rs.getInt("id"));
				cartorio.setCnpj(rs.getString("cnpj"));
				cartorio.setNome(rs.getString("nome"));
				cartorio.setDocumento(rs.getString("documento"));
				cartorio.setTelefone(rs.getInt("telefone"));
				cartorio.setSituacaoCad(rs.getInt("situacaoCad"));
				cartorio.setHorarioFunc(rs.getString("horarioFunc"));
				cartorio.setNumero(rs.getInt("numero"));
				cartorio.setCEP(rs.getInt("cep"));
				cartorio.setLogradouro(rs.getString("logradouro"));
				cartorio.setBairro(rs.getString("bairro"));
				cartorio.setCidade(rs.getString("cidade"));
				cartorio.setEstado(rs.getString("estado"));
				
					if (rs.getString("horarioFunc") != null) {
						
					
					lista.add(cartorio);
					
					}
			
				}
				} catch (SQLException e) {
				e.printStackTrace();
				}
				} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
				}
				return lista;
				}
			
			
			public ArrayList<Cartorio> listarCartorios(String chave) {
				System.out.println("entrou");
				Cartorio cartorio;
				ArrayList<Cartorio> lista = new ArrayList<>();
				String sqlSelect = "\"SELECT id, cnpj, nome, documento, telefone, situacaoCad, horarioFunc, site, numero, cep, logradouro, bairro, cidade, estado FROM cartorio where upper(nome) like ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
				stm.setString(1, "%" + chave.toUpperCase() + "%");
				try (ResultSet rs = stm.executeQuery();) {
				while (rs.next()) {
					
					cartorio = new Cartorio();
					cartorio.setId(rs.getInt("id"));
					cartorio.setCnpj(rs.getString("cnpj"));
					cartorio.setNome(rs.getString("nome"));
					cartorio.setDocumento(rs.getString("documento"));
					cartorio.setTelefone(rs.getInt("telefone"));
					cartorio.setSituacaoCad(rs.getInt("situacaoCad"));
					cartorio.setHorarioFunc(rs.getString("horarioFunc"));
					cartorio.setSite(rs.getString("site"));
					cartorio.setNumero(rs.getInt("numero"));
					cartorio.setCEP(rs.getInt("cep"));
					cartorio.setLogradouro(rs.getString("logradouro"));
					cartorio.setBairro(rs.getString("bairro"));
					cartorio.setCidade(rs.getString("cidade"));
					cartorio.setEstado(rs.getString("estado"));
					
					if (rs.getString("horarioFunc") != null) {
						
					
					lista.add(cartorio);
					
					}
				}
				} catch (SQLException e) {
				e.printStackTrace();
				}
				} catch (SQLException e1) {
				System.out.print(e1.getStackTrace());
				}
				return lista;
				}
			
			
		}	
		
			
			

