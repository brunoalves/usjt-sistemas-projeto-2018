<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- Importando o header -->
<c:import url="header.jsp" />
<main class="row">
<form method="post" action="methods/login.jsp"
	class="col-md-4 col-centered animated fadeInUp">
	<h1>Login de Usuário</h1>
	<input type="text" placeholder="usuário" required="required"> <input
		type="password" placeholder="senha" required="required">
	<button type="submit">Entrar</button>
	<a href="#">Não possui cadastro?</a>
</form>
</main>

<!-- Importando o Footer -->
<c:import url="footer.jsp" />