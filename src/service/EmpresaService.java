package service;

import model.Empresa;


import java.util.ArrayList;

import dao.EmpresaDAO;

public class EmpresaService {
	
	EmpresaDAO dao = new EmpresaDAO();
		
		public int criar(Empresa empresa) {
			return dao.criar(empresa);
		}
		
		public void atualizar(Empresa empresa){
			dao.atualizar(empresa);
		}
		
		public void excluir(int id){
			dao.excluir(id);
		}
		
		public Empresa carregar(Empresa empresa){
			return dao.carregar(empresa);
		}
		
		

		public ArrayList<Empresa> listarEmpresas(){
		return dao.listarEmpresas();
		} public ArrayList<Empresa> listarEmpresas(String chave){
		return dao.listarEmpresas(chave);
		}
		

		

	}



