<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	<title>Power Guido</title>
</head>
	<body class="cadastrar-empresa">
		<c:import url="header-empresa.jsp" />
			<main>
			
			 <!-- Modal -->
            <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="modalLabel">Excluir Empresa</h4>
                        </div>
                        <div class="modal-body">
                            Deseja realmente excluir esta empresa?
                        </div>
                        <div class="modal-footer">
                            <form action="ManterEmpresa.do" method="post">
                                <input type="hidden" name="id" id="id_excluir" />
                                <button type="submit"  name="acao" value="Excluir">Sim</button>
                                <button type="button"  data-dismiss="modal">N&atilde;o</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal -->
				<h1>Lista de empresas cadastradas</h1>
				<div id="main" class="container">
                <form action="listar_empresas.do" method="post">
                    <div id="top" class="row">
                     

                        <div class="col-md-6">
                            <div class="input-group h2">
                                <input name="data[search]" class="form-control" id="search" type="text" placeholder="Pesquisar">
                                <span class="input-group-btn" style="text-align: left; width: 60px; float: left;">
                				<button class="btn btn-primary" type="submit" name="acao" value="buscar">
                				<!-- <span class="glyphicon glyphicon-search"></span>-->
                    			<img src="https://res.abc.net.au/bundles/2.1.0/images/icon-search-grey@1x.png">
                                </button>
                                </span>
                            </div>
                        </div>

                        
                    </div>
                    <!-- /#top -->
                </form>
                
                
                   <c:if test="${not empty lista}">
                <div id="list" class="row">

                    <div class="table-responsive col-md-12">
                        <table class="table table-striped" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>CNPJ</th>
                                    <th>Razão Social</th>
                                    <th>Nome</th>
                                    <th>Telefone</th>
                                    <th>Email</th>
                                    <th class="actions">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
          					<c:forEach var="empresa" items="${lista }">
                                       <tr>
                                            <td>
                                               ${empresa.id }
                                            </td>
                                            <td>
                                                ${empresa.cnpj }
                                            </td>
                                            <td>
                                                ${empresa.razaoSocial}
                                            </td>
                                            <td>
                                                ${empresa.nome}
                                            </td>
                                             <td>
                                                ${empresa.telefone}
                                            </td>
                                             <td>
                                                ${empresa.email}
                                            </td>
                                            <td class="actions">
                                               
                                                <button style="margin-top: 0;" href="ManterPais.do?acao=Editar&id=${empresa.id }">Editar</button>
                                                <button style="margin-top: 0;" id="btn${empresa.id }%>" type="button"  data-toggle="modal" data-target="#delete-modal" data-cliente="${empresa.id }">Excluir</button>
                                            </td>
                                        </tr>
                            </c:forEach>

                            </tbody>
                        </table>

                    </div>
                </div>
                <!-- /#list -->

                <div id="bottom" class="row">
                    <!-- <div class="col-md-12">
                        <ul class="pagination">
                            <li class="disabled"><a>&lt; Anterior</a>
                            </li>
                            <li class="disabled"><a>1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li class="next"><a href="#" rel="next">Próximo &gt;</a>
                            </li>
                        </ul>
                    </div>-->
                </div>
                </c:if>
			</main>
		</div>
		<footer>
			<script src="js/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/main.js"></script>
		</footer>
	</body>
</html>
