package service;

import model.Cartorio;
import model.Empresa;

import java.util.ArrayList;

import dao.CartorioDAO;

public class CartorioService {
	
	CartorioDAO dao = new CartorioDAO();
		
		public int criar(Cartorio cartorio) {
			return dao.criar(cartorio);
		}
		
		public void atualizar(Cartorio cartorio){
			dao.atualizar(cartorio);
		}
		
		public void excluir(int id){
			dao.excluir(id);
		}
		
		public Cartorio carregar(Cartorio cartorio){
			return dao.carregar(cartorio);
		}
	
		public ArrayList<Cartorio> listarCartorios(){
			return dao.listarCartorios();
			} public ArrayList<Cartorio> listarCartorios(String chave){
			return dao.listarCartorios(chave);
			}

}



