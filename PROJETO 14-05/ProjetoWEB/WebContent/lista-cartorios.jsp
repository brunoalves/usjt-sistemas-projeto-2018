<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	<title>Power Guido</title>
</head>
	<body class="cadastrar-empresa">
		<header id="lateral-menu-bar">
			<nav>
				<ul class="menu">
					<li><a href="home-cartorio.jsp"><img src="images/icon-add.png" alt=""><span>Adicionar cart�rio</span></a></li>
					<li class="ativo"><a href="#"><img src="images/icon-menu.png" alt=""><span>Listar cart�rios</span></a></li>
				</ul>
			</nav>
		</header>
		<div class="content">
			<header id="top-nav">
				<div class="row">
					<div class="col-6"></div>
					<div class="col-6 pull-right">
						<div class="informations">
							<img src="images/user.png" alt="">
							<span>Usu�rio Empresa</span>
						</div>
					</div>
				</div>
			</header>
			<main>
				<h1>Lista de cart�rios cadastradas</h1>
				<div class="box col-md-12">
					<table>
						<thead>
							<td>Nome</td>
							<td>A��es</td>
						</thead>
						<tr>
							<td>Cart�rio Bolsomais</td>
							<td>Remover</td>
						</tr>
						<tr>
							<td>Cart�rio Geraldo Alquimia</td>
							<td>Remover</td>
						</tr>
						<tr>
							<td>Cart�rio Lu�s Ign�cio preso na cela</td>
							<td>Remover</td>
						</tr>
					</table>
				</div>
			</main>
		</div>
		<footer>
			<script src="js/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/main.js"></script>
		</footer>
	</body>
</html>
