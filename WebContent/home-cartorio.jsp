<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Importando o header -->
<c:import url="header-cartorio.jsp" />

<!-- Conteúdo desta página -->
<main>
<h1>Cadastrar Cartório</h1>
<div class="row">
	<div id="terms" class="col-md-12 box animated fadeInRight">
		<h3>Termos de Adesão</h3>
		<p>Bacon ipsum dolor amet spare ribs andouille ham, t-bone turkey
			buffalo strip steak. Prosciutto meatloaf pork chop doner tail
			frankfurter, rump hamburger ham. Salami filet mignon corned beef
			short loin. Kevin short loin sirloin, picanha ham hock meatball cow.

			Meatball brisket frankfurter tri-tip turducken shankle. Pork chop
			flank andouille sirloin landjaeger tri-tip. Tongue salami meatloaf
			kevin chuck swine. Boudin hamburger drumstick, spare ribs pork loin
			jowl alcatra ham hock. Hamburger shankle brisket filet mignon pork,
			pork loin kielbasa cupim shoulder bresaola spare ribs ham hock short
			loin bacon. Sausage spare ribs rump boudin pancetta pig. Cow capicola
			pig meatball cupim. Filet mignon kevin prosciutto, pork loin strip
			steak salami cupim sausage tongue. Prosciutto swine pork belly
			burgdoggen. Beef ribs ground round ball tip burgdoggen frankfurter
			venison sausage andouille turducken pastrami jowl porchetta. Capicola
			chuck burgdoggen pork, pig tri-tip bacon pastrami bresaola jowl
			corned beef pork chop. Pig tongue hamburger shank burgdoggen, strip
			steak porchetta. Cupim meatloaf fatback porchetta bresaola sausage.

			Filet mignon porchetta tri-tip pancetta t-bone chicken. Short loin
			beef pancetta, buffalo pastrami meatloaf pork belly rump shoulder
			t-bone turkey turducken bresaola salami. Porchetta pork loin brisket
			turducken pork chop cupim t-bone ham hock kielbasa tenderloin sausage
			kevin ground round cow jowl. Jowl pork belly beef ribs shank. Cow
			shankle salami, fatback ground round short loin shank pork belly
			strip steak drumstick pastrami spare ribs venison swine. Kielbasa
			biltong pastrami tri-tip. Flank strip steak pork belly, spare ribs
			burgdoggen pancetta drumstick meatloaf beef jowl ribeye t-bone
			alcatra. Sirloin landjaeger t-bone buffalo chuck, porchetta filet
			mignon fatback prosciutto bacon boudin swine rump. Cupim meatball ham
			beef, fatback tenderloin kevin spare ribs short loin. Ham beef
			landjaeger kielbasa cow swine short ribs shankle andouille. Pastrami
			prosciutto brisket buffalo corned beef filet mignon tongue kielbasa
			frankfurter pork belly. Tongue short ribs capicola, picanha kevin
			boudin prosciutto ham spare ribs chicken jerky shank sausage jowl.
			Bacon biltong chicken turkey pork belly beef boudin tail short loin
			tri-tip turducken. Pork loin tri-tip beef chicken prosciutto spare
			ribs landjaeger cupim sirloin jerky porchetta. Jowl turkey pork
			belly, chicken pancetta meatloaf tongue buffalo tri-tip beef
			burgdoggen. Chuck shank sausage, drumstick pork belly tri-tip
			tenderloin picanha ham hock pork loin buffalo brisket hamburger
			alcatra pork chop. Beef ribs chuck jerky landjaeger, shankle buffalo
			picanha prosciutto frankfurter turducken hamburger. Doner jerky
			boudin alcatra, tongue swine kielbasa pork chop chicken t-bone
			hamburger frankfurter shankle. Tongue tail capicola kevin porchetta
			shank leberkas. Ground round tenderloin picanha tail jowl. Short ribs
			capicola pork chop, kevin pig cow flank tenderloin pork loin beef
			ribs doner t-bone prosciutto shoulder. Capicola hamburger turducken
			ham, venison flank brisket prosciutto. Turducken picanha bresaola,
			doner cow pork chop filet mignon porchetta pancetta frankfurter spare
			ribs chuck. Ribeye ground round sausage porchetta doner leberkas
			swine. Pork loin bresaola biltong cow, alcatra doner prosciutto
			t-bone porchetta bacon pork chop pork belly venison frankfurter
			kielbasa. Ham hock shoulder tri-tip filet mignon, kielbasa t-bone
			tenderloin prosciutto short loin shank pastrami flank chicken brisket
			hamburger. Leberkas meatloaf pork chop, picanha beef ribs shankle
			chicken meatball rump. Salami pork chop jowl, shoulder tenderloin
			short loin biltong kielbasa turducken andouille cupim. Meatball shank
			short ribs cow jerky filet mignon brisket picanha. Boudin meatball
			turducken shankle tri-tip swine biltong shank drumstick. Kevin pork
			chop chicken shankle hamburger tri-tip, venison porchetta rump strip
			steak picanha pancetta swine. Cow ham shank short loin turducken
			bacon tri-tip ball tip. Jowl brisket pastrami salami filet mignon
			pork belly tenderloin. Chuck shank sausage filet mignon cupim
			meatloaf jowl pancetta flank. Jerky brisket strip steak sausage doner
			pig shank picanha biltong boudin short loin. Spare ribs landjaeger
			beef, strip steak meatloaf fatback rump pork chop sausage porchetta.
			Turkey corned beef buffalo landjaeger flank capicola, cupim short
			loin beef ribs shank pastrami. Short loin salami fatback ground round
			landjaeger pig chicken flank ball tip picanha bresaola ribeye pork
			chop. Tongue cupim pig, pork bresaola drumstick landjaeger chuck
			filet mignon jerky tenderloin flank meatball short ribs. Porchetta
			chicken landjaeger, strip steak turkey pancetta tail ribeye t-bone
			spare ribs swine pastrami short ribs filet mignon venison. Pork tail
			turkey chicken capicola. Beef ribs hamburger capicola, cupim ball tip
			turducken t-bone ground round doner ham hock pastrami buffalo venison
			landjaeger. Prosciutto spare ribs ham pork loin salami, pancetta ham
			hock. Pork belly turducken alcatra shankle meatball cupim, fatback
			chuck landjaeger bacon. Landjaeger pancetta ribeye spare ribs
			prosciutto jerky strip steak leberkas meatball cow pork chop. Jowl
			pork pork loin ball tip frankfurter, alcatra drumstick. Ground round
			leberkas t-bone short loin pork chop bresaola pig cupim boudin short
			ribs swine. Chicken biltong jowl shank. Porchetta sausage tenderloin
			filet mignon. Spare ribs meatball drumstick cupim, chicken bresaola
			pork ham hock pork belly short loin alcatra t-bone strip steak.
			Turkey turducken pastrami, short loin pancetta beef ribs shankle
			ground round tail bacon burgdoggen venison ham. Boudin drumstick
			pastrami pork belly, pig hamburger chuck sausage jerky biltong.
			Turducken spare ribs meatloaf leberkas ham porchetta. Capicola
			frankfurter picanha hamburger spare ribs. Corned beef turkey salami
			landjaeger buffalo, hamburger kevin meatloaf. Shankle hamburger
			drumstick, turducken venison capicola pancetta pork ribeye chuck
			sirloin doner ham. Prosciutto beef ribs fatback, pig chuck sirloin
			pastrami cupim short ribs meatball turkey shoulder hamburger pork
			belly boudin. Pork chop chuck hamburger prosciutto. T-bone ribeye
			boudin tail turkey. Biltong pork belly capicola frankfurter, sausage
			kielbasa cow andouille. Pork chop brisket filet mignon flank.
			Kielbasa pork chop prosciutto jowl. Pastrami hamburger swine tail
			capicola rump sirloin ribeye strip steak. Prosciutto pork belly short
			loin venison meatball ball tip, jerky hamburger chicken cow. Beef
			ribs swine beef, flank turducken spare ribs tongue venison. Pig
			tri-tip sirloin shankle, porchetta short ribs turkey meatball jowl
			cow pastrami beef ribs. Pastrami swine t-bone, pork chop chicken
			turkey picanha sirloin tongue brisket andouille shankle porchetta
			doner. Jerky bacon pastrami boudin short loin. Biltong tenderloin
			shankle, ball tip fatback tail spare ribs kevin meatball pancetta
			burgdoggen chicken kielbasa. Boudin alcatra turkey shank bacon
			fatback short ribs. Bresaola tongue swine shoulder leberkas cupim ham
			flank short loin, jerky tenderloin pork belly. Turkey filet mignon
			pig, pancetta pork biltong salami tongue kevin sirloin pastrami
			leberkas hamburger flank. Filet mignon andouille alcatra kevin swine
			ribeye frankfurter sirloin bacon t-bone biltong meatball burgdoggen
			bresaola rump. Bresaola picanha buffalo, hamburger capicola fatback
			short loin flank beef pastrami strip steak. Turkey pork ribeye
			tri-tip sausage brisket pork chop pork belly biltong. Prosciutto
			brisket picanha pork loin, ham hock porchetta meatball rump alcatra
			beef ribs cupim bresaola tenderloin shank sausage. Buffalo pancetta
			shank hamburger doner boudin sirloin, brisket beef. Sausage shoulder
			filet mignon corned beef meatloaf kielbasa meatball swine pancetta
			venison turkey pastrami. Leberkas burgdoggen flank rump. Spare ribs
			filet mignon short ribs sausage pork loin. Venison pig tail cupim
			chicken. Burgdoggen shank turducken tail drumstick short ribs beef
			flank chuck doner swine pork loin tri-tip chicken ball tip. Pork loin
			t-bone hamburger kielbasa corned beef tenderloin ground round pork
			chop kevin filet mignon buffalo cow shank frankfurter pork. Cupim ham
			ball tip turkey short ribs landjaeger, strip steak corned beef
			brisket tenderloin pork biltong. Tenderloin kevin t-bone landjaeger
			beef ribs frankfurter kielbasa chuck biltong turducken shankle
			porchetta. Buffalo meatloaf boudin, biltong strip steak kielbasa pork
			chop short loin filet mignon fatback ham hock flank. Picanha short
			loin pork belly fatback sirloin shank frankfurter, tenderloin turkey
			bresaola jowl pastrami turducken. Strip steak turkey t-bone, biltong
			tongue rump meatloaf jowl kevin spare ribs pig tri-tip meatball
			tenderloin. Shankle ball tip venison meatball salami doner
			frankfurter. Short loin pork chop salami tri-tip pork belly. Does
			your lorem ipsum text long for something a little meatier? Give our
			generator a try… it’s tasty!</p>
		<div class="terms-end">
			<input id="terms-check" type="checkbox"><label>Li e
				aceito os termos de uso</label>
		</div>

	</div>
	<div id="after-terms">
		<div class="col-md-12 box">
			<h3>Dados do Cartório:</h3>
			<form action="#">
				<input type="text" class="col-md-3" required="required"
					placeholder="CNPJ"> <input type="text" class="col-md-4"
					required="required" placeholder="Razão Social"> <input
					type="text" class="col-md-4" required="required"
					placeholder="Endereço"> <input type="text" class="col-md-3"
					required="required" placeholder="Bairro"> <input
					type="text" class="col-md-4" required="required" placeholder="CEP">
				<input type="text" class="col-md-4" required="required"
					placeholder="Cidade"> <input type="text" class="col-md-3"
					required="required" placeholder="Estado"> <input
					type="file" class="col-md-4" required="required"
					placeholder="Documento"> <input type="text"
					class="col-md-4" required="required" placeholder="E-mail">
				<button type="submit" class="pull-right">Cadastrar</button>
			</form>
		</div>
	</div>
</div>
</main>
<!-- Importando o Footer -->
<c:import url="footer.jsp" />