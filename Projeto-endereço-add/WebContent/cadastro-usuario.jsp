<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Animate -->
<link rel="stylesheet" href="css/animate.css">

<!-- Main Style -->
<link rel="stylesheet" href="style.css">
<title>Power Soft - Cadastro de usuário</title>
</head>

	<body class="cadastrar-empresa">
		<header id="top-nav">
			<div class="row">
				<div class="col-6"></div>
				<div class="col-6 pull-right">
					<div class="informations">
						<!-- <img src="images/user.png" alt=""> -->
						<a href="index.jsp">Voltar</a>
					</div>
				</div>
			</div>
		</header>
		
		
		
		
		
		
		
		<main>
		<div class="row">
		<style>
			.cadastrar-empresa input{
				width: 100%;
			}
			.cadastrar-empresa select{
				font-size: 16px;
				padding: 10px;
				border-radius: 20px;
				background-color: #091b26;
				border: 0;
				color: #fff;
				margin: 0 auto;
			    display: block;
			    margin-bottom: 10px;
			}
		</style>
			<div class="col-lg-6 col-md-8 col-centered box">
				<h1>Cadastro de usuário</h1>
	<form class="row" action="methods/registro.jsp">			
				<p>Tipo de usuário:</p>
	<select name="tipo-usuario" id="">
		<option value="">Selecione o tipo de usuário</option>	
		<option value="empresa">Empresa</option>
		<option value="cartorio">Cartório</option>
	</select>
				<!--<h3 style="color: #10bdd0;">Selecione o seu tipo de usuário:</h3>-->
				
					<input type="text" name="documento" required="required" class="col-lg-6" placeholder="Documento">
					<input type="password" name="senha" required="required" class="col-lg-12" placeholder="Senha">
					<input type="email" name="email" required="required" class="col-lg-12" placeholder="E-mail">
					<!-- <input type="password" value="" required="required" class="col-lg-12" placeholder="Repita a senha"> -->
					<button type="submit" class="col-lg-3">Cadastrar</button>
				</form>
			</div>
		</div>
	</main>
	<!-- Importando o Footer -->
	<c:import url="footer.jsp" />