	package model;

import java.io.Serializable;

public class Cartorio implements Serializable {
	private static final long serialVersionUID = 1L;
	
		private int id;
		private String cnpj;
		private String nome;
		private String documento;
		private int telefone;
		private int situacaoCad;
		
		//n�o obrigatorios
		private String horarioFunc;
		private String site;
		

		
		public Cartorio() {
		}
		
		
		
		public int getId() {
			return id;
		}



		public void setId(int id) {
			this.id = id;
		}



		public String getCnpj() {
			return cnpj;
		}



		public void setCnpj(String cnpj) {
			this.cnpj = cnpj;
		}
		
		
		
		public String getNome() {
			return nome;
		}



		public void setNome(String nome) {
			this.nome = nome;
		}


		
		public int getTelefone() {
			return telefone;
		}



		public void setTelefone(int telefone) {
			this.telefone = telefone;
		}

		
		
		public String getHorarioFunc() {
			return horarioFunc;
		}

		
		
		public void setHorarioFunc(String horarioFunc) {
			this.horarioFunc = horarioFunc;
		}

		
		
		public String getSite() {
			return site;
		}

		
		
		public void setSite(String site) {
			this.site = site;
		}


		@Override
		public String toString() {
			return "Id= " + id + ", \nC.N.P.J= " + cnpj +  "\nNome= " + nome 
					+ "\nTelefone= " + telefone + horarioFunc +  "\nHorario de funcionamento= "
					+ site +  "\nSite= " + "\n-------------";
		}



		public String getDocumento() {
			return documento;
		}



		public void setDocumento(String documento) {
			this.documento = documento;
		}



		public int getSituacaoCad() {
			return situacaoCad;
		}



		public void setSituacaoCad(int situacaoCad) {
			this.situacaoCad = situacaoCad;
		}


		
		
			


		
		
		
		


		
		





		
}	