SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`administrador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`administrador` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) ,
  `documento` VARCHAR(13) NOT NULL,
  `senha` VARCHAR(50) NOT NULL,
  `situacaoCad` VARCHAR(1) NOT NULL,
  `razaoSocial` VARCHAR(45),
  `email` VARCHAR(45) ,
  `site` VARCHAR(45) NULL,
  `dtAbert` DATETIME NULL,
  `descEcoJur` VARCHAR(45) NULL,
  `numero` INT(20) ,
  `CEP` INT(8),
  `logradouro` VARCHAR(45),
  `bairro` VARCHAR(45),
  `cidade` VARCHAR(45),
  `estado` VARCHAR(2) ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`admNewUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`admNewUser` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `administrador_id` INT NOT NULL,
  `nomeUsuario` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(13) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_admNewUser_administrador1_idx` (`administrador_id` ASC),
  CONSTRAINT `fk_admNewUser_administrador1`
    FOREIGN KEY (`administrador_id`)
    REFERENCES `mydb`.`administrador` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = euckr;


-- -----------------------------------------------------
-- Table `mydb`.`cartorio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`cartorio` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cnpj` VARCHAR(14) ,
  `nome` VARCHAR(45) ,
  `documento` VARCHAR(13) NOT NULL,
  `situacaoCad` VARCHAR(1) NOT NULL,
  `telefone` INT ,
  `horarioFunc` DATETIME NULL,
  `site` VARCHAR(45) NULL,
  `numero` INT ,
  `senha` VARCHAR(50) NOT NULL,
  `CEP` INT(8) ,
  `logradouro` VARCHAR(45) ,
  `bairro` VARCHAR(45) ,
  `cidade` VARCHAR(45) ,
  `estado` VARCHAR(2) ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`empresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cnpj` VARCHAR(14) ,
  `senha` VARCHAR(50) NOT NULL,
  `situacaoCad` VARCHAR(1) NOT NULL,
  `razaoSocial` VARCHAR(45) ,
  `nome` VARCHAR(45) ,
  `documento` VARCHAR(13) NOT NULL,
  `telefone` VARCHAR(20),
  `email` VARCHAR(45),
  `numero` INT ,
  `CEP` INT(8) ,
  `logradouro` VARCHAR(45) ,
  `bairro` VARCHAR(45) ,
  `cidade` VARCHAR(45) ,
  `estado` VARCHAR(2) ,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cartNewUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`cartNewUser` (
  `id` INT NOT NULL,
  `nomeUsuario` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(13) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `cartorio_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cartNewUser_cartorio1_idx` (`cartorio_id` ASC),
  CONSTRAINT `fk_cartNewUser_cartorio1`
    FOREIGN KEY (`cartorio_id`)
    REFERENCES `mydb`.`cartorio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = euckr;


-- -----------------------------------------------------
-- Table `mydb`.`EmpNewUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`EmpNewUser` (
  `id` INT NOT NULL,
  `empresa_id` INT NOT NULL,
  `nomeUsuario` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(13) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_EmpNewUser_empresa1_idx` (`empresa_id` ASC),
  CONSTRAINT `fk_EmpNewUser_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `mydb`.`empresa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = euckr;


-- -----------------------------------------------------
-- Table `mydb`.`tabeliao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tabeliao` (
  `id` INT NOT NULL,
  `cartorio_id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(13) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tabeliao_cartorio1_idx` (`cartorio_id` ASC),
  CONSTRAINT `fk_tabeliao_cartorio1`
    FOREIGN KEY (`cartorio_id`)
    REFERENCES `mydb`.`cartorio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`subTabeliao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`subTabeliao` (
  `id` INT NOT NULL,
  `tabeliao_id` INT NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `documento` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_subTabeliao_tabeliao1_idx` (`tabeliao_id` ASC),
  CONSTRAINT `fk_subTabeliao_tabeliao1`
    FOREIGN KEY (`tabeliao_id`)
    REFERENCES `mydb`.`tabeliao` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- SELECT*FROM empresa; 
SELECT*FROM administrador;
 -- SELECT*FROM cartorio;

-- USE `mydb` ;
-- drop database mydb;
-- delete from usuario where id='3';
-- INSERT INTO usuariodocumento, senha, situacaoCad) VALUES ('123', '123', '1');
 -- UPDATE empresa SET situacaoCad=1 where id=1;
 -- INSERT INTO administrador (id,nome, documento,senha,situacaoCad,razaoSocial,email,site,dtAbert,descEcoJur, numero, CEP, logradouro, bairro, cidade, estado) VALUES ('1','Thanos Downey Parker','345622789489','morteparametade','1','Exercito Negro LTDA','thanos_joias@gmail.com','www.wikihow.com/pegar-joias-infinito','28/04/18','Procuramos todas as joias do infinito','666','08958666','Praça do equilibrio','Vila Joias Divinas','Vormir','VM');
