package service;

import model.Administrador;
import dao.AdministradorDAO;

public class AdministradorService {
	
	AdministradorDAO dao = new AdministradorDAO();
		
		public int criar(Administrador administrador) {
			return dao.criar(administrador);
		}
		
		public void atualizar(Administrador administrador){
			dao.atualizar(administrador);
		}
		
		public void excluir(int id){
			dao.excluir(id);
		}
		
		public Administrador carregar(Administrador administrador){
			return dao.carregar(administrador);
		}
	}



