package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Empresa;

import service.EmpresaService;


/**
 * Servlet implementation class ManterPaisController
 */
@WebServlet("/ManterEmpresa.do")
public class ManterEmpresaController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pAcao = request.getParameter("acao");
		String pId = request.getParameter("id");
		String Ecnpj = request.getParameter("cnpj");
		String ErazaoSocial = request.getParameter("razaoSocial");
		String Enome = request.getParameter("nome");
		String Edocumento = request.getParameter("documento");
		int Etelefone = Integer.parseInt(request.getParameter("telefone"));
		String Eemail = request.getParameter("email");
		
		int id = -1;
		
		try {
			id = Integer.parseInt(pId);
			} catch (NumberFormatException e) {
			}
		
		//instanciar o javabean
		Empresa empresa = new Empresa();
		empresa.setId(id);
		empresa.setCnpj(Ecnpj);
		empresa.setRazaoSocial(ErazaoSocial);
		empresa.setNome(Enome);
		empresa.setDocumento(Edocumento);
		empresa.setTelefone(Etelefone);
		empresa.setEmail(Eemail);

		
		//instanciar o service
		EmpresaService es = new EmpresaService();
		es.criar(empresa);
		empresa = es.carregar(empresa);

		
		RequestDispatcher view = null;
		HttpSession session = request.getSession();
		
		
		if (pAcao.equals("Excluir")) {
			es.excluir(empresa.getId());
			ArrayList<Empresa> lista = (ArrayList<Empresa>) session .getAttribute( "lista" ) ;
			lista.remove(busca(empresa, lista));
			session.setAttribute("lista", lista);
			view = request.getRequestDispatcher("ListarPaises.jsp");
			} else if (pAcao.equals("Alterar")) {
			es.atualizar(empresa);
			ArrayList<Empresa> lista = (ArrayList<Empresa>) session .getAttribute( "lista" ) ;
			int pos = busca(empresa, lista);
			lista.remove(pos);
			lista.add(pos, empresa);
			session.setAttribute("lista", lista);
			request.setAttribute("empresa", empresa);
			view = request.getRequestDispatcher("lista-empresas.jsp");
			} else if (pAcao.equals("Editar")) {
			empresa = es.carregar(empresa);
			request.setAttribute("cliente", empresa);
			view = request.getRequestDispatcher("lista-empresas.jsp");
			}
			view.forward(request, response);
			}
	
	
	public int busca(Empresa empresa, ArrayList<Empresa> lista) {
		Empresa to;
		for(int i = 0; i < lista.size(); i++){
		to = lista.get(i);
		if(to.getId() == empresa.getId()){
		return i;
		}
		}
		return -1;
		}

	}
	




