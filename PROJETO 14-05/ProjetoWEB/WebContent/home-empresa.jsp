<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- Importando o header -->
<c:import url="header-empresa.jsp" />
<!-- Conteúdo desta página -->

<main>
	<h1>Cadastrar Empresa</h1>
	<div class="row">
		<div id="terms" class="col-md-12 box animated fadeInRight">
			<h3>TERMOS E CONDIÇÕES GERAIS DE USO DO SITE “POWER GUIDE”</h3>
			<p> Estes termos e condições gerais de uso aplicam-se aos serviços prestados pela pessoa jurídica Power Guide™ LTDA, devidamente registrada sob o CNPJ n. 28.074.122/0001-58, com sede em:
Rua das Flores - N° 1234
Edifício Florido, Bloco das Margaridas - sala 43
Bairro Jardins
São Paulo – SP
CEP: 03216-020
Por meio do site Power Guide, com o seguinte endereço: http://power-guide.com.br 

1. DO OBJETO 

O site Power Guide caracteriza-se pela prestação dos seguintes serviços:

Disponibilização de cadastro de empresas e cartórios, sistema no qual viabiliza o upload e download de arquivos contábeis.

2. DA ACEITAÇÃO DOS TERMOS E CONDIÇÕES GERAIS DE USO 

Todos aqueles que desejarem ter acesso aos serviços ofertados através do site Power Guide deverão, primeiramente, se informar sobre as regras que compõem o presente instrumento, as quais ficarão disponíveis para pronta e ampla consulta, em link direto no próprio site.

Ao utilizar o site Power Guide, o usuário aceita integralmente as presentes normas e compromete-se a observá-las, sob risco de aplicação das penalidades cabíveis. Antes de iniciar qualquer navegação no site, o usuário deverá cientificar-se de eventuais modificações ou atualizações ocorridas nestes termos.
Caso não concorde com quaisquer das regras aqui descritas, o usuário deve, imediatamente, abster-se de utilizar o serviço. Se for de seu interesse, poderá, ainda, entrar em contato com o serviço de atendimento ao cliente, para apresentar-lhe as suas ressalvas.

3. DA NAVEGAÇÃO

O editor do site Power Guide se compromete a utilizar todas as soluções técnicas à sua disposição para permitir o acesso ao serviço 24 (vinte e quatro) horas por dia, 7 (sete) dias por semana. Entretanto, ele poderá, a qualquer momento, interromper, limitar ou suspender o acesso ao site ou a algumas de suas páginas, a fim de realizar atualizações, modificações de conteúdo ou qualquer outra ação julgada necessária para o seu bom funcionamento.

Os presentes termos e condições gerais de uso se aplicam a todas as extensões do site Power Guide em redes sociais ou em comunidades, tanto as já existentes, quanto aquelas ainda a serem implementadas.

4. DA GESTÃO DO SITE

Para a boa gestão, o editor do site Power Guide poderá, a qualquer momento:

a) suspender, interromper ou limitar o acesso a todo ou a parte do site a uma categoria específica de internautas;

b) remover toda informação que possa perturbar o funcionamento do site ou que estejam em conflito com normas de Direito brasileiro ou de Direito internacional;

c) suspender o site, a fim de realizar atualizações e modificações.

5. DO CADASTRO

Os serviços disponibilizados no site Power Guide apenas poderão ser acessados por pessoas plenamente capazes, conforme o Direito brasileiro. Todos aqueles que não possuírem plena capacidade civil - menores de 18 anos não emancipados, pródigos, ébrios habituais ou viciados em tóxicos e pessoas que não puderem exprimir sua vontade, por motivo transitório ou permanente - deverão ser devidamente assistidos por seus representantes legais, que se responsabilizarão pelo cumprimento das presentes regras.

Ao usuário, será permitido manter apenas uma conta junto ao site Power Guide. Contas duplicadas serão automaticamente desativadas pelo editor do site, sem prejuízo de demais penalidades cabíveis.

Para o devido cadastramento junto ao serviço, o usuário deverá fornecer integralmente os dados requeridos. Todas as informações fornecidas pelo usuário devem ser precisas, verdadeiras e atualizadas. Em qualquer caso, o usuário responderá, em âmbito cível e criminal, pela veracidade, exatidão e autenticidade dos dados informados.

O usuário deverá fornecer um endereço de e-mail válido, através do qual o site realizará todos os contatos necessários. Todas as comunicações enviadas para o referido endereço serão consideradas lidas pelo usuário, que se compromete, assim, a consultar regularmente suas mensagens recebidas e a respondê-las em prazo razoável.

Após a confirmação de seu cadastro, o usuário possuirá um login (apelido) e uma senha pessoais, que deverão ser por ele utilizados para o acesso a sua conta no site Power Guide. Estes dados de conexão não poderão ser informados pelo usuário a terceiros, sendo de sua inteira responsabilidade o uso que deles seja feito. O usuário compromete-se a comunicar imediatamente ao editor do site quaisquer atividades suspeitas ou inesperadas em sua conta.

Não será permitido ceder, vender, alugar ou transferir, de qualquer forma, a conta.

Será automaticamente descadastrado o usuário que descumprir quaisquer das normas contidas no presente instrumento, sendo-lhe vedado realizar nova inscrição no site.

O usuário poderá, a qualquer tempo e sem necessidade de justificação, requerer o cancelamento de seu cadastro junto ao site Power Guide. O seu descadastramento será realizado o mais rapidamente possível, desde que não sejam verificados débitos em aberto.


6. DAS RESPONSABILIDADES

O editor se responsabilizará pelos defeitos ou vícios encontrados nos serviços prestados pelo site Power Guide, desde que tenha lhes dado causa. Defeitos ou vícios técnicos ou operacionais originados no próprio sistema do usuário não serão de responsabilidade do editor.

O editor responsabiliza-se apenas pelas informações que foram por ele diretamente divulgadas. Quaisquer informações incluídas pelos usuários, tais como em comentários e em perfis pessoais, serão de inteira responsabilidade dos próprios.

O usuário é responsável, ainda:

a) pela correta utilização do site e de seus serviços, prezando pela boa convivência, pelo respeito e pela cordialidade no relacionamento com os demais usuários;

b) pelo cumprimento das regras contidas neste instrumento, bem como normas de Direito nacional e de Direito internacional;

c) pela proteção dos dados de acesso à sua conta (login e senha).

O editor não será responsável:

a) pelas características intrínsecas da internet, principalmente relativas à confiabilidade e à procedência das informações circulando nesta rede;

b) pelos conteúdos ou atividades ilícitas praticadas através de seu site.

7. DOS LINKS EXTERNOS

O site Power Guide pode conter links externos redirigindo o usuário para outras páginas da internet, sobre os quais o editor não exerce controle. Apesar das verificações prévias e regulares realizadas pelo editor, ele se isenta de qualquer responsabilidade sobre o conteúdo encontrado nestes sites e serviços.
Poderão ser incluídos links nas páginas e nos documentos do site Power Guide, desde que não sirvam para fins comerciais ou publicitários. Esta inclusão dependerá de autorização prévia do editor.

Não será autorizada a inclusão de páginas que divulguem quaisquer tipos de informações ilícitas, violentas, polêmicas, pornográficas, xenofóbicas, discriminatórias ou ofensivas.

8. DOS DIREITOS AUTORAIS

A estrutura do site Power Guide, bem como os textos, os gráficos, as imagens, as fotografias, os sons, os vídeos e as demais aplicações informáticas que o compõem são de propriedade do editor e são protegidas pela legislação brasileira e internacional referente à propriedade intelectual.

Qualquer representação, reprodução, adaptação ou exploração parcial ou total dos conteúdos, marcas e serviços propostos pelo site, por qualquer meio que seja, sem autorização prévia, expressa e escrita do editor, é estritamente vedada, podendo-se recorrer às medidas cíveis e penais cabíveis. Estão excluídos desta previsão apenas os elementos que estejam expressamente designados no site como livres de direitos autorais.

O acesso não gera para o usuário qualquer direito de propriedade intelectual relativo a elementos do site, os quais restam sob a propriedade exclusiva do editor.

É vedado ao usuário incluir no site dados que possam modificar o seu conteúdo ou sua aparência.

9. DA POLÍTICA DE PRIVACIDADE

1. Dados pessoais

Todos os dados pessoais fornecidos pelo usuário no momento de seu cadastro ou que venham a ser solicitados posteriormente, a este título, permanecerão sigilosos e não serão repassados a quaisquer parceiros do site.
O editor apenas poderá transmitir os dados pessoais a terceiros quando o usuário ao permitir expressamente ou quando presentes as hipóteses legais previstas no Direito brasileiro, tais como a requisição judicial.

É de responsabilidade do editor a garantia de confidencialidade dos dados pessoais fornecidos pelos usuários, devendo proteger o site contra tentativas de violações ou acessos clandestinos à sua base de dados.

2. Dados de navegação

O site recorre eventualmente às técnicas de "cookies", que lhe permitem analisar as estatísticas e as informações sobre a navegação do usuário. 

Podem ser fornecidos, por exemplo, dados sobre o dispositivo utilizado pelo usuário e o seu local de acesso. Esta coleta de informações busca melhorar a navegação, para o conforto do usuário, ao permitir apresentar-lhe serviços personalizados, de acordo com suas preferências.

Estes dados de navegação poderão, ainda, ser compartilhados com eventuais parceiros do site, buscando o aprimoramento dos produtos e serviços ofertados ao usuário. 

O usuário poderá se opor ao registro de "cookies" pelo site, bastando desativar esta opção no seu próprio navegador ou aparelho. Por outro lado, a desativação deste registro poderá afetar a disponibilidade de algumas ferramentas e alguns serviços do site.

10. DO SERVIÇO DE ATENDIMENTO AO USUÁRIO

Em caso de dúvidas, sugestões ou problemas com a utilização do site Power Guide, o usuário poderá contatar diretamente o seu serviço de atendimento, através do endereço de e-mail: sac@power-guide.com.br, bem como do telefone: (11) 4002-8922. Estes serviços de atendimento ao usuário estarão disponíveis nos seguintes dias e horários:

De segunda à sábado, das 10h00 às 18h00; 
de segunda à sexta-feira, das 07h00 às 21h00; 
24 (vinte e quatro) horas por dia, 7 (sete) dias por semana.

O usuário poderá, ainda, optar por enviar correspondência ao endereço da sede do site Power Guide, informado no início deste documento.

11. DAS SANÇÕES

Sem prejuízo das demais medidas legais cabíveis, o editor do site Power Guide poderá, a qualquer momento, advertir, suspender ou cancelar a conta do usuário:

a) que descumprir quaisquer dos dispositivos contidos no presente instrumento;

b) que descumprir os seus deveres de usuário;

c) que praticar atos fraudulentos ou dolosos;

d) cujo comportamento constitua ou possa vir a importar ofensa ou danos a terceiros ou ao próprio site.

12. DAS ALTERAÇÕES

A presente versão dos termos e condições gerais de uso foi atualizada pela última vez em: 01/03/2018.

O editor se reserva o direito de modificar, a qualquer momento e sem qualquer aviso prévio, o site e os serviços, bem como as presentes normas, especialmente para adaptá-las às evoluções do site Power Guide, seja pela disponibilização de novas funcionalidades, seja pela supressão ou modificação daquelas já existentes.

É de total direito do usuário pedir, imediatamente, o cancelamento de sua conta e apresentar a sua ressalva ao serviço de atendimento, se assim o desejar.

13. DO DIREITO APLICÁVEL E DO FORO

Para a solução das controvérsias decorrentes do presente instrumento, será aplicado integralmente o Direito brasileiro.

Os eventuais litígios deverão ser apresentados no foro da comarca em que se encontra a sede do editor do site.

Seja bem-vindo(a)!
A equipe do site Power Guide lhe deseja uma excelente navegação!




</p>
			<div class="terms-end">
				<input id="terms-check" type="checkbox"><label>Li e aceito os termos de uso</label>
			</div>

		</div>
		<div id="after-terms">
			<div class="col-md-12 box">
				<h3>Dados da empresa:</h3>
				<form action="methods/registroEmp.jsp">
					<input type="text" class="col-md-3" required="required" placeholder="CNPJ">
					<input type="text" class="col-md-4" required="required" placeholder="Razão Social">
					<input type="text" class="col-md-4" required="required" placeholder="Endereço"> 
					<input type="text" class="col-md-3" required="required" placeholder="Bairro"> 
					<input type="text" class="col-md-4" required="required" placeholder="CEP">
					<input type="text" class="col-md-4" required="required" placeholder="Cidade"> 
					<input type="text" class="col-md-3" required="required" placeholder="Estado"> 
					<input type="file" class="col-md-4" required="required" placeholder="Documento"> 
					<input type="text" class="col-md-4" required="required" placeholder="E-mail">
					<button type="submit" class="pull-right">Cadastrar</button>
				</form>
			</div>
		</div>
	</div>
</main>
<!-- Importando o Footer -->
<c:import url="footer.jsp" />