package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Administrador;


public class AdministradorDAO {
	

	

		//METODO C R U D
			
			public int criar(Administrador administrador) {
				String sqlInsert = "INSERT INTO administrador(nome, documento,razaoSocial, site, dataAbertura,descEcoJur) "
						+ "VALUES (?, ?, ?, ?, ?, ?)";
			
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlInsert);) {
					stm.setString(1, administrador.getNome());
					stm.setString(2, administrador.getDocumento());
					stm.setString(3, administrador.getRazaoSocial());
					stm.setString(4, administrador.getSite());
					stm.setInt(5, administrador.getDataAbertura());
					stm.setString(6, administrador.getDescEcoJur());
					stm.execute();
					String sqlQuery  = "SELECT LAST_INSERT_ID()";
					try(PreparedStatement stm2 = conn.prepareStatement(sqlQuery);
						ResultSet rs = stm2.executeQuery();) {
						if(rs.next()){
							administrador.setId(rs.getInt(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				return administrador.getId();
			}
			
			public void excluir(int id) {
				String sqlDelete = "DELETE FROM administrador WHERE id = ?";
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlDelete);) {
					stm.setInt(1, id);
					stm.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			public void atualizar(Administrador administrador) {
				String sqlUpdate = "UPDATE administrador SET nome=?, documento=?,razaoSocial=?, site=?, dataAbertura=?,descEcoJur=? "
						+ " WHERE id=?";
				// usando o try with resources do Java 7, que fecha o que abriu
				try (Connection conn = ConnectionFactory.obtemConexao();
				PreparedStatement stm = conn.prepareStatement(sqlUpdate);) {
					stm.setString(1, administrador.getNome());
					stm.setString(2, administrador.getDocumento());
					stm.setString(3, administrador.getRazaoSocial());
					stm.setString(4, administrador.getSite());
					stm.setInt(5, administrador.getDataAbertura());
					stm.setString(6, administrador.getDescEcoJur());

					stm.setInt(4, administrador.getId());
				stm.execute();
				} catch (Exception e) {
				e.printStackTrace();
				}
				}
			
			
			
			public Administrador carregar(Administrador administrador) {
				String sqlSelect = "SELECT nome, documento,razaoSocial, site, dataAbertura,descEcoJur"
						+ "  FROM Administrador WHERE Administrador.id = ?";
				
				try (Connection conn = ConnectionFactory.obtemConexao();
						PreparedStatement stm = conn.prepareStatement(sqlSelect);) {
					stm.setInt(1, administrador.getId());
					try (ResultSet rs = stm.executeQuery();) {
						if (rs.next()) {
							administrador.setNome(rs.getString("nome"));
							administrador.setDocumento(rs.getString("nome"));
							administrador.setRazaoSocial(rs.getString("nome"));
							administrador.setSite(rs.getString("nome"));
							administrador.setDataAbertura(rs.getInt(213123127));
							administrador.setEmail(rs.getString("nome"));

						} else {
							administrador.setId(-1);
							administrador.setNome(null);

							
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
				} catch (SQLException e1) {
					System.out.print(e1.getStackTrace());
			}
				return administrador;
			}
			
			
			
		}	
		
			
			

