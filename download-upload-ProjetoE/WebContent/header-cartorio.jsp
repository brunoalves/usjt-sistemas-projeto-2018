<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="model.Usuario"%>

<!DOCTYPE html>

<%
	// verificando se tem um atributo login na sessao
	// se tiver vai continuar e mostrar o menu
	if(session.getAttribute("documento") != null) {
%>

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Animate -->
<link rel="stylesheet" href="css/animate.css">

<!-- Main Style -->
<link rel="stylesheet" href="style.css">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

<title>Power Soft</title>
</head>
<body class="cadastrar-cartorio">
	<%
	// if ((session.getAttribute("usuario") == null) || (session.getAttribute("usuario") == "")) {
%>

	<!-- <div class="content">
		<h1>
			Você não está logado, por favor inice uma sessão <a href="index.jsp">aqui</a>.
		</h1>
	</div>-->

	<%//} else { %>
	<header id="lateral-menu-bar">
		<nav>
			<ul class="menu">
				<li class="ativo"><a href="home-cartorio.jsp"><img
						src="images/icon-add.png" alt="Adicionar"><span>Adicionar
							cartorio</span></a></li>
				<li><a href="lista-cartorios.jsp"><img
						src="images/icon-menu.png" alt="Listar"><span>Listar
							cartorios</span></a></li>
			</ul>
		</nav>
	</header>
	<div class="content">
		<header id="top-nav">
			<div class="row">
				<div class="col-6"></div>
				<div class="col-6 pull-right">
					<div class="informations">
						<img src="images/user.png" alt=""> <span>Usuário
							cartorio <% out.print(session.getAttribute("documento"));%></span> 
			
			<!--  		  <a href="login.jsp?acao=logout">Logout</a>-->
					</div>
				</div>
			</div>
		</header>
	<%
	// se não existir um login na sessao, 
	// vai enviar para a página de login novamente
	} else {
		response.sendRedirect("index.jsp");
	}

%>

</head>
<body>