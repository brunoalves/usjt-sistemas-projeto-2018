jQuery(document).ready(function($) {
	var docH = $(document).height();
	var windowH = $(window).height();

	$('.login-page').height(windowH);
	$('#lateral-menu-bar').height(docH);
	
	$('#after-terms').hide();
	$('#terms-check').click(function(event) {
		$('#after-terms').slideToggle();
	});
});