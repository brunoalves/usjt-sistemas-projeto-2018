<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- Animate -->
<link rel="stylesheet" href="css/animate.css">

<!-- Main Style -->
<link rel="stylesheet" href="style.css">

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

<title>Power Soft</title>
</head>
<body class="login-page">
<main class="row">
<form method="post" action="methods/login.jsp"
	class="col-md-4 col-centered animated fadeInUp">
	<h1>Login de Usuário</h1>
	<style>
		select{
			font-size: 16px;
			padding: 10px;
			border-radius: 20px;
			background-color: #091b26;
			border: 0;
			color: #fff;
			margin: 0 auto;
		    display: block;
		    margin-bottom: 10px;
		}
	</style>
	<p>Tipo de usuário:</p>
	<select name="tipo-usuario" id="">
		<option value="">Selecione o tipo de usuário</option>
		<option value="administrador">Administrador</option>
		<option value="empresa">Empresa</option>
		<option value="cartorio">Cartório</option>
	</select>
	<input type="text" placeholder="documento" name="documento" required="required"> <input
		type="password" placeholder="senha" name="senha" required="required">
	<button type="submit">Entrar</button>
	<a href="cadastro-usuario.jsp">Não possui cadastro?</a>
</form>
</main>

<!-- Importando o Footer -->
<link href="style.css" rel="stylesheet" type="text/css">
<c:import url="footer.jsp"/>